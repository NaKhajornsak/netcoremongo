﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetCoreMongo.Models
{
    public class CustomerDataAccess : DataAccess
    {
        private readonly IMongoCollection<Customer> customerCollection;
        public CustomerDataAccess()
        {
            var filter = new BsonDocument("name", "customer");
            var collections = _db.ListCollectionsAsync(new ListCollectionsOptions { Filter = filter }).Result;

            if (!collections.AnyAsync().Result)
            {
                _db.CreateCollection("customer");
            }
            customerCollection = _db.GetCollection<Customer>("customer");
        }
        //public IEnumerable<Customer> GetCustomers()
        //{
        //    var filter = new BsonDocument("name", "customer");

        //    //return _db.GetCollection<Customer>("customer").Find(_ => true).ToList();

        //    return _collection.Find(_ => true).ToList();
        //}

        public async Task<IEnumerable<Customer>> GetCustomers(int pageNo, int pageSize)
        {
            pageNo = pageNo <= 0 ? 1 : pageNo; // default =1;
            pageSize = (pageSize < 0 || pageSize > 200) ? 50 : pageSize; // default = 50;

            return await customerCollection.Find(_ => true).Skip(pageNo - 1 * pageSize).Limit(pageSize).ToListAsync();
        }

        public async Task<string> Create(Customer cust)
        {
            cust.Id = ObjectId.GenerateNewId();
            await customerCollection.InsertOneAsync(cust);

            return cust.Id.ToString();
        }


        public async Task<bool> Update(Customer cust)
        {
            //var r = await customerCollection.UpdateOneAsync(cust);
            var r = await customerCollection.ReplaceOneAsync(o => o.Id == cust.Id, cust);
            return r.IsAcknowledged;
        }

        public async Task<bool> Remove(string id)
        {
            ObjectId objId = new ObjectId(id);
            var r = await customerCollection.DeleteOneAsync(o => o.Id == objId);

            return r.IsAcknowledged;
        }
    }
}
