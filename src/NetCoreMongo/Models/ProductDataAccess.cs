﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetCoreMongo.Models
{
    public class ProductDataAccess : DataAccess
    {
        public ProductDataAccess()
        {

        }

        public  IEnumerable<Product> GetProducts()
        {
            return _db.GetCollection<Product>("product").Find(_ => true).ToList();
        }

        public async Task<IEnumerable<Product>> GetAllProducts()
        {
            return await _db.GetCollection<Product>("product").Find(_ => true).ToListAsync();
        }
    }
}
