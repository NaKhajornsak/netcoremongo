﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetCoreMongo.Models
{
    public class Product
    {
        public ObjectId Id { get; set; }
        [BsonElement("Pid")]
        public string Pid { get; set; }
        [BsonElement("Name")]
        public string Name { get; set; }
        [BsonElement("Price")]
        public double Price { get; set; }
        [BsonElement("Package")]
        public bool Package { get; set; }

    }
}
