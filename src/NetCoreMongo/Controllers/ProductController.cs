﻿using Microsoft.AspNetCore.Mvc;
using NetCoreMongo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetCoreMongo.Controllers
{
    [Route("api/[controller]")]
    public class ProductController: Controller
    {
        //public IEnumerable<Product> GetProducts()
        //{
        //    return new ProductDataAccess().GetProducts();
        //}

        public async Task<IEnumerable<Product>> GetAllProducts()
        {
            return await new ProductDataAccess().GetAllProducts();
        }
    }
}
