﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NetCoreMongo.Models;
using NetCoreMongo.Models.Repository;

namespace NetCoreMongo.Controllers
{
    [Route("api/[controller]")]
    public class CustomerController : Controller
    {
        //public CustomerController(ICustomerRepository custRepo)
        //{
        //    custRepo = custRepo;
        //}
        [HttpGet]
        public async Task<IEnumerable<Customer>> GetCustomers()
        {
            return await new CustomerDataAccess().GetCustomers(1, 100);
        }

        [HttpPut]
        public async Task<bool> UpdateCustomer(Customer cust)
        {
            return await new CustomerDataAccess().Update(cust);
        } 

        [HttpPost]
        //public async Task<string> CreateCustomer([FromBody]Customer cust)
        public async Task<IActionResult> CreateCustomer([FromBody]Customer cust)
        {
            if (cust == null) return BadRequest();
            //return await new CustomerDataAccess().Create(cust);
            try
            {
                string r = await new CustomerDataAccess().Create(cust);
                return Json(new { status="ok", message = r});
            }
            catch (Exception)
            {
                return Json(new { status = "error", message = "error creating customer" });
            }
        }

        [HttpDelete]
        public async Task<bool> DeleteCustomer(string id)
        {
            return await new CustomerDataAccess().Remove(id);
        }
    }
}
